/*
 * AdbuddizUtils.h
 *
 *  Created on: Dec 22, 2015
 *      Author: anil
 */

#ifndef ADBUDDIZUTILS_H_
#define ADBUDDIZUTILS_H_

#include "cocos2d.h"

USING_NS_CC;

class AdbuddizUtils {
public:
	AdbuddizUtils();
	virtual ~AdbuddizUtils();

	void static showAd();
	void static showRewardVideo();
};

#endif /* ADBUDDIZUTILS_H_ */
