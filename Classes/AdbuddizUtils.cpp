/*
 * AdbuddizUtils.cpp
 *
 *  Created on: Dec 22, 2015
 *      Author: anil
 */

#include "AdbuddizUtils.h"
#include <jni.h>
#include <platform/android/jni/JniHelper.h>

AdbuddizUtils::AdbuddizUtils() {
	// TODO Auto-generated constructor stub

}

AdbuddizUtils::~AdbuddizUtils() {
	// TODO Auto-generated destructor stub
}

void AdbuddizUtils::showAd() {
	cocos2d::JniMethodInfo methodInfo;
		if (JniHelper::getStaticMethodInfo(methodInfo,
				"org/cocos2dx/cpp/AppActivity", "showAd", "()V")) {

			methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
					methodInfo.methodID);
			methodInfo.env->DeleteLocalRef(methodInfo.classID);
		}
}

void AdbuddizUtils::showRewardVideo() {
	cocos2d::JniMethodInfo methodInfo;
		if (JniHelper::getStaticMethodInfo(methodInfo,
				"org/cocos2dx/cpp/AppActivity", "showRewardedVideo", "()V")) {

			methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
					methodInfo.methodID);
			methodInfo.env->DeleteLocalRef(methodInfo.classID);
		}
}
