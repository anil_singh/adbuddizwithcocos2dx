#include "HelloWorldScene.h"
#include "cocos2d.h"
#include "AdbuddizUtils.h"

USING_NS_CC;

Scene* HelloWorld::createScene() {
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = HelloWorld::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init() {
	//////////////////////////////
	// 1. super init first
	if (!Layer::init()) {
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	/////////////////////////////
	// 2. add a menu item with "X" image, which is clicked to quit the program
	//    you may modify it.

	// add a "close" icon to exit the progress. it's an autorelease object
	auto closeItem = MenuItemImage::create("CloseNormal.png",
			"CloseSelected.png",
			CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));

	closeItem->setPosition(
			Vec2(
					origin.x + visibleSize.width
							- closeItem->getContentSize().width / 2,
					origin.y + closeItem->getContentSize().height / 2));
	closeItem->setTag(0);

	// create menu, it's an autorelease object
	auto menu = Menu::create(closeItem, NULL);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu, 1);

	/////////////////////////////
	// 3. add your codes below...

	// add a label shows "Hello World"
	// create and initialize a label

	auto label = Label::createWithTTF("Hello World", "fonts/Marker Felt.ttf",
			24);

	// position the label on the center of the screen
	label->setPosition(
			Vec2(origin.x + visibleSize.width / 2,
					origin.y + visibleSize.height
							- label->getContentSize().height));

	// add the label as a child to this layer
	this->addChild(label, 1);

	// add "HelloWorld" splash screen"
	auto sprite = Sprite::create("HelloWorld.png");

	// position the sprite on the center of the screen
	sprite->setPosition(
			Vec2(visibleSize.width / 2 + origin.x,
					visibleSize.height / 2 + origin.y));

	// add the sprite as a child to this layer
//	this->addChild(sprite, 0);

//	PerkUtills::trackEvent("f1b3f72be6a3fb4dbe2c2a5f1c2d584a88e07cf2");

	

	MenuItemFont *adUnitMenu = MenuItemFont::create("Show Ad",
			CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));
	adUnitMenu->setTag(1);
	MenuItemFont *displayUnitAdMenu = MenuItemFont::create(
			"Show RewardVideoAd",
			CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));
	displayUnitAdMenu->setTag(2);
	

	Menu *menu1 = Menu::create( adUnitMenu, displayUnitAdMenu, NULL);
	menu1->alignItemsVerticallyWithPadding(10);
	menu1->setPosition(visibleSize.width / 2, visibleSize.height/2);
	addChild(menu1);

	return true;
}

void HelloWorld::menuCloseCallback(Ref* pSender) {
	int tag = ((MenuItem*) pSender)->getTag();

	switch (tag) {
	case 0:
		Director::getInstance()->end();
		break;
	case 1:
		AdbuddizUtils::showAd();
		break;
	case 2:
	AdbuddizUtils::showRewardVideo();
		break;
	default:
		break;
	}

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}
